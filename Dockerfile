FROM ubuntu:rolling AS installer
RUN apt update
RUN apt install -y curl openssl xxd

RUN curl --output /tmp/tresorit_installer.run https://installer.tresorit.com/tresorit_installer.run

COPY check_signature.sh /tmp/check_signature.sh
RUN /tmp/check_signature.sh /tmp/tresorit_installer.run

ARG CACHE_DATE=_

RUN sh /tmp/tresorit_installer.run --update /
RUN ls /tresorit

COPY entrypoint.sh /tresorit/entrypoint.sh


FROM ubuntu:rolling

RUN apt-get update \
 && apt-get install -y cron fuse rsync \
 && apt-get clean \
 && rm -rf /var/cache/apt/archives /var/lib/apt/lists/*

RUN useradd --user-group --system --create-home --no-log-init tresorit
USER tresorit
ENV PATH="${PATH}:/tresorit/"

COPY --from=installer --chown=tresorit:tresorit /tresorit /tresorit

ENTRYPOINT [ "entrypoint.sh" ]

