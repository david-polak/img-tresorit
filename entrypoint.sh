#!/usr/bin/env bash
set -e

echo "Starting tresorit-daemon"
tresorit-cli start
tresorit-cli status --porcelain | grep running

term() {
  echo "Stopping tresorit-daemon"
  tresorit-cli stop
}
trap 'term' TERM

tail --pid="$(pgrep tresorit-daemon)" -f /tresorit/Logs/* &
wait $!
